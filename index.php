<?php
// set show all errors
error_reporting(E_ALL);
ini_set('display_errors',1);

require_once dirname(__FILE__).'/vendor/autoload.php';
require_once dirname(__FILE__).'/lib/function.php';
?>
<!DOCTYPE html>
<html>
<head>
    <title>Test feed instagram data By Tag name Netclubyo</title>
    <link rel="stylesheet" href="./assets/plugin/bootstrap-4.0.0/dist/css/bootstrap.min.css" crossorigin="anonymous">
</head>
<body>
<?php

$limit_photo = 50;
$hash_tag ='BNK48';

Unirest\Request::verifyPeer(false); // not confirm ssl

$instagram = new \InstagramScraper\Instagram();
$medias = $instagram->getMediasByTag($hash_tag, $limit_photo);

$list_instagrams = [];
if (! empty($medias) AND is_array($medias)) {
    foreach ($medias as $results) {
        $list_instagrams[] = [
            'id' => $results->getId(),
            'create_at' => $results->getCreatedTime(),
            'shotrcode' => $results->getShortCode(),
            'caption' => $results->getCaption(),
            'comments' => $results->getCommentsCount(),
            'likes' => $results->getLikesCount(),
            'href' => $results->getLink(),
            'image' => $results->getImageStandardResolutionUrl()
        ];
    }
}

?>

<header>
  <div class="collapse bg-dark" id="navbarHeader">
    <div class="container">
      <div class="row">
        <div class="col-sm-8 col-md-7 py-4">
          <h4 class="text-white">About</h4>
          <p class="text-muted">Add some information about the album below, the author, or any other background context. Make it a few sentences long so folks can pick up some informative tidbits. Then, link them off to some social networking sites or contact information.</p>
        </div>
        <div class="col-sm-4 offset-md-1 py-4">
          <h4 class="text-white">Contact</h4>
          <ul class="list-unstyled">
            <li><a href="#" class="text-white">Follow on Twitter</a></li>
            <li><a href="#" class="text-white">Like on Facebook</a></li>
            <li><a href="#" class="text-white">Email me</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="navbar navbar-dark bg-dark box-shadow">
    <div class="container d-flex justify-content-between">
      <a href="#" class="navbar-brand d-flex align-items-center">
        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><path d="M23 19a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h4l2-3h6l2 3h4a2 2 0 0 1 2 2z"></path><circle cx="12" cy="13" r="4"></circle></svg>
        <strong>Album</strong>
      </a>
    </div>
  </div>
</header>

    <main role="main">

      <section class="jumbotron text-center">
        <div class="container">
          <h1 class="jumbotron-heading">Instagram Feed Photo By Tag Name Example Netclubyo</h1>
          <p class="lead text-muted">Something short and leading about the collection below—its contents, the creator, etc. Make it short and sweet, but not too short so folks don't simply skip over it entirely.</p>
        </div>
      </section>

      <div class="album py-5 bg-light">
        <div class="container">

                <div class="row">
                <?php if (! empty($list_instagrams)) : ?>
                    <?php foreach ($list_instagrams as $info) : ?>  
                    <div class="col-md-4">
                      <div class="card mb-4 box-shadow">
                        <a href="<?php echo $info['href']; ?>" target="_blank">
                            <img class="card-img-top" src="<?php echo $info['image']; ?>" alt="Card image cap">
                        </a>
                        <div class="card-body">
                          <p class="card-text"><?php echo $info['caption']; ?></p>
                          <div class="d-flex justify-content-between align-items-center">
                            <div class="btn-group">
                              <a target="_blank" class="btn btn-sm btn-outline-secondary" href="<?php echo $info['href']; ?>">Read more</a>
                            </div>
                            <small class="text-muted">Likes <?php echo $info['likes']; ?> &nbsp&nbsp|&nbsp&nbsp Comment <?php echo $info['comments']; ?></small>
                          </div>
                        </div>
                      </div>
                    </div>
                    <?php endforeach; ?>
                <?php endif; ?>
                </div>
        </div>
      </div>

    </main>

    <footer class="text-muted">
      <div class="container">
        <p class="float-right">
          <a href="#">Back to top</a>
        </p>
        <p>Album example is &copy; Bootstrap, but please download and customize it for yourself!</p>
        <p>New to Bootstrap? <a href="../../">Visit the homepage</a> or read our <a href="../../getting-started/">getting started guide</a>.</p>
      </div>
    </footer>


<!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="./assets/plugin/bootstrap-4.0.0/dist/js/bootstrap.min.js" crossorigin="anonymous"></script>

</body>
</html>